using System;
using Xunit;
using System.Collections.Generic;
using System.Collections;
using Rectangles.Models;
using System.ComponentModel.DataAnnotations;

namespace RectanglesUnitTests
{
    public class RectanglesTest : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] {
                new Rectangle(){Width = Math.Sqrt(8), Length = Math.Sqrt(8)},
                4d,
                8d,
                TypeRectangle.Square
            };

            yield return new object[] {
                new Rectangle(){Width = 4, Length = 2},
                4.472136d,
                8d,
                TypeRectangle.Tall
            };

            yield return new object[] {
                new Rectangle(){Width = 2, Length = 4},
                4.472136d,
                8d,
                TypeRectangle.Flat
            };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class TestRectangle
    {
        [Theory]
        [ClassData(typeof(RectanglesTest))]
        public void TestRectangleAPI(Rectangle rectangle, double diagonal, double area, TypeRectangle type)
        {
            Assert.Equal(diagonal, rectangle.Diagonal(), 6);
            Assert.Equal(area, rectangle.Area(), 6);
            Assert.Equal(type.ToString(), rectangle.Type());
        }
    }
}