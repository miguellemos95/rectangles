using System;
using Xunit;
using Rectangles.Services;
using System.Collections.Generic;
using System.Collections;
using Rectangles.Models;

namespace RectanglesUnitTests
{
    public class SortRectanglesTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] {
                null,
                null
            };

            yield return new object[] {
                new List<Rectangle>(),
                new List<Rectangle>()
            };

            yield return new object[] {
                new List<Rectangle>(){
                    new Rectangle(){Width = 2, Length = 1}
                },
                new List<Rectangle>(){
                    new Rectangle(){Width = 2, Length = 1}
                }
            };

            yield return new object[] {
                new List<Rectangle>(){
                    new Rectangle(){Width = 1, Length = 1},
                    new Rectangle(){Width = 2, Length = 4},
                    new Rectangle(){Width = 3, Length = 1}
                },
                new List<Rectangle>(){
                    new Rectangle(){Width = 1, Length = 1},
                    new Rectangle(){Width = 3, Length = 1},
                    new Rectangle(){Width = 2, Length = 4}
                },
            };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }


    public class TestSortRectanglesService
    {
        [Theory]
        [ClassData(typeof(SortRectanglesTestData))]
        public void TestSortRectangles(List<Rectangle> input, List<Rectangle> sorted)
        {
            SortRectanglesService sortRectanglesService = new SortRectanglesService();
           
            List<Rectangle> result = sortRectanglesService.SortRectangles(input);
            Assert.Equal(result, sorted);
        }
    }
}
