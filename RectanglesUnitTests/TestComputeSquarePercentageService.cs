using System;
using Xunit;
using Rectangles.Services;
using System.Collections.Generic;
using System.Collections;
using Rectangles.Models;

namespace RectanglesUnitTests
{
    public class RectanglesTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] {
                null,
                -1d
            };

            yield return new object[] {
                new List<Rectangle>(),
                -1d
            };

            yield return new object[] {
                new List<Rectangle>(){
                    new Rectangle(){Width = 2, Length = 2}
                },
                100d
            };

            yield return new object[] {
                new List<Rectangle>(){
                    new Rectangle(){Width = 1, Length = 1},
                    new Rectangle(){Width = 2, Length = 4},
                    new Rectangle(){Width = 3, Length = 1},
                    new Rectangle(){Width = 3, Length = 4}
                },
                25d
            };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class TestComputeSquarePercentageService
    {
        [Theory]
        [ClassData(typeof(RectanglesTestData))]
        public void TestComputeSquarePercentage(List<Rectangle> input, double squarePercentage)
        {
            ComputeSquarePercentageService computeSquarePercentageService = new ComputeSquarePercentageService();
           
            double result = computeSquarePercentageService.ComputeSquarePercentage(input);
            Assert.Equal(result, squarePercentage);
        }
    }

}
