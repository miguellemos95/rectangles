using Microsoft.AspNetCore.Mvc;
using Rectangles.Dtos;
using Rectangles.Models;
using Rectangles.Services;
using Rectangles.Data.Repositories;
using AutoMapper;
using System.Collections.Generic;

namespace Rectangles.Controllers
{
    [Route("rectangles")]
    [ApiController]
    public class RectangleController : ControllerBase
    {
        private readonly IRectangleRepository _repository;
        private readonly ISortRectanglesService _sortRectanglesService;
        private readonly IComputeSquarePercentageService _computeSquarePercentageService;
        private readonly IMapper _mapper;

        public RectangleController(IRectangleRepository repository, ISortRectanglesService sortRectanglesService, IComputeSquarePercentageService computeSquarePercentageService, IMapper mapper)
        {
            _repository = repository;
            _sortRectanglesService = sortRectanglesService;
            _computeSquarePercentageService = computeSquarePercentageService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<RectangleInfoDto> GetRectangles()
        {
            List<Rectangle> rectangles = _repository.GetRectangles();
            
            return Ok(_mapper.Map<IEnumerable<RectangleInfoDto>>(rectangles));
        }

        [HttpPost]
        public ActionResult <PostRectanglesResponseDto> PostRectangle(PostRectanglesRequestDto postRectanglesRequestDto)
        {
            List<Rectangle> rectanglesList = _mapper.Map<List<Rectangle>>(postRectanglesRequestDto.Rectangles);
            
            _sortRectanglesService.SortRectangles(rectanglesList);
            double squarePercentage = _computeSquarePercentageService.ComputeSquarePercentage(rectanglesList);
            
            _repository.CreateRectangles(rectanglesList);

            PostRectanglesResponseDto rectangleInfoDtos = _mapper.Map<List<Rectangle>, PostRectanglesResponseDto>(rectanglesList);
            rectangleInfoDtos = _mapper.Map<double, PostRectanglesResponseDto>(squarePercentage, rectangleInfoDtos);
            return Ok(rectangleInfoDtos);
        }
    }
}