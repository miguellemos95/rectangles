using System.Collections.Generic;
using AutoMapper;
using Rectangles.Dtos;
using Rectangles.Models;

namespace Rectangles.Profiles
{
    public class RectangleProfile : Profile
    {
        public RectangleProfile()
        {
            CreateMap<RectangleDto, Rectangle>();
            CreateMap<Rectangle, RectangleInfoDto>();
            
            CreateMap<List<Rectangle>, PostRectanglesResponseDto>().ForMember(dest => dest.Rectangles, opt => opt.MapFrom(source => source));
            CreateMap<double, PostRectanglesResponseDto>().ForMember(dest => dest.PercentSquare, opt => opt.MapFrom(source => source));
        }
    }
}