using System.Collections.Generic;

namespace Rectangles.Dtos
{
    public class PostRectanglesResponseDto
    {
        public List<RectangleInfoDto> Rectangles { get; set; }

        public double PercentSquare {get; set;}      
    }   
}