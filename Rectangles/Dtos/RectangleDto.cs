using System.ComponentModel.DataAnnotations;
using Rectangles.Services;

namespace Rectangles.Dtos
{
    public class RectangleDto
    {
        [Required]
        [Range(double.Epsilon, double.MaxValue, ErrorMessage = "The length of rectangle is invalid!")]
        public double Length {get; set;}
        
        [Required]
        [Range(double.Epsilon, double.MaxValue, ErrorMessage = "The width of rectangle is invalid!")]
        public double Width {get; set;}       
    }   
}