using System.Collections.Generic;

namespace Rectangles.Dtos
{
    public class PostRectanglesRequestDto
    {
        public List<RectangleDto> Rectangles { get; set; }
    }   
}