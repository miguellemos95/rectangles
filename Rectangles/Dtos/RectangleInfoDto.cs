using Rectangles.Models;
using static Rectangles.Models.Rectangle;

namespace Rectangles.Dtos
{
    public class RectangleInfoDto
    {
        public double Length{get; set;}
        public double Width{get; set;}
        public string Type {get; set;}
        public double Area {get; set;}
        public double Diagonal {get; set;}
    }   
}