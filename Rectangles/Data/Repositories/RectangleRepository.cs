using Rectangles.Models;
using System.Collections.Generic;

namespace Rectangles.Data.Repositories
{
    public class RectangleRepository : IRectangleRepository
    {
        private List<Rectangle> Rectangles = new List<Rectangle>();

        public void CreateRectangles(List<Rectangle> Rectangles)
        {
           this.Rectangles.AddRange(Rectangles);
        }

        public List<Rectangle> GetRectangles()
        {
            return Rectangles;
        }
    }
}
        