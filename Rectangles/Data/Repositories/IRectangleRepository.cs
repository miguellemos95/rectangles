using System.Collections.Generic;
using Rectangles.Models;


namespace Rectangles.Data.Repositories
{
    public interface IRectangleRepository
    {
        List<Rectangle> GetRectangles();
        void CreateRectangles(List<Rectangle> Rectangles);
    }
}