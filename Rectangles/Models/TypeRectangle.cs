namespace Rectangles.Models
{
    public enum TypeRectangle
    {
        Tall, 
        Flat,
        Square
    }
}