using System;
using System.ComponentModel.DataAnnotations;
using Rectangles.Services;

namespace Rectangles.Models
{
    public class Rectangle : IComparable<Rectangle>
    {
        [Required]
        [Range(double.Epsilon, double.MaxValue, ErrorMessage = "The length of rectangle is invalid!")]
        public double Length {get; set;}
        
        [Required]
        [Range(double.Epsilon, double.MaxValue, ErrorMessage = "The Width of rectangle is invalid!")]
        public double Width {get; set;}
        public TypeRectangle TypeRectangle {get; set;}
        private double CachedDiagonal = 0;
        
        public double Diagonal()
        {
            if(CachedDiagonal == 0)
            {
                CachedDiagonal = Math.Sqrt(Math.Pow(Length, 2) + Math.Pow(Width, 2));
            }
            
            return CachedDiagonal;
        }

        public double Area()
        {
            return Length * Width;
        }

        public string Type()
        {
            if(Length < Width)
            {
                TypeRectangle = TypeRectangle.Tall;
                return TypeRectangle.Tall.ToString();
            }

            if(Length > Width)
            {
                TypeRectangle = TypeRectangle.Flat;
                return TypeRectangle.Flat.ToString();
            }

            TypeRectangle = TypeRectangle.Square;
            return TypeRectangle.Square.ToString();
        }

        public int CompareTo(Rectangle other)
        {
            return this.Diagonal().CompareTo(other.Diagonal());
        }
    }   
}