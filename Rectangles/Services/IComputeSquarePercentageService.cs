using System.Collections.Generic;
using Rectangles.Models;

namespace Rectangles.Services
{
    public interface IComputeSquarePercentageService
    {
       double ComputeSquarePercentage(List<Rectangle> rectanglesList);
    }
}
