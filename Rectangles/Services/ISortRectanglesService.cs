using System.Collections.Generic;
using Rectangles.Models;

namespace Rectangles.Services
{
    public interface ISortRectanglesService
    {
        List<Rectangle> SortRectangles(List<Rectangle> rectanglesList); 
    }
}
