using System.Collections.Generic;
using Rectangles.Models;

namespace Rectangles.Services
{
    public class SortRectanglesService : ISortRectanglesService
    {
        public List<Rectangle> SortRectangles(List<Rectangle> rectanglesList)
        {
            if(rectanglesList == null)
            {
                return null;
            }

            rectanglesList.Sort();
            return rectanglesList;
        }
    }
}