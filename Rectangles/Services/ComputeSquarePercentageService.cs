using System.Collections.Generic;
using Rectangles.Models;

namespace Rectangles.Services
{
    public class ComputeSquarePercentageService : IComputeSquarePercentageService
    {
       public double ComputeSquarePercentage(List<Rectangle> rectanglesList)
       {
           if(rectanglesList == null || rectanglesList.Count == 0)
           {
               return -1;
           }
            
            int squareCount = 0;
            foreach(Rectangle rectangle in rectanglesList)
            {
                if(rectangle.Type() == TypeRectangle.Square.ToString())
                {
                    squareCount++;
                }
            }
               
            return squareCount * 100.0d / rectanglesList.Count;
        }   
    }
}